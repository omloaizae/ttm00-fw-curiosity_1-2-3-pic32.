/* 
 * File:   main.c
 * Author: nb1
 *
 * Created on July 10, 2019, 7:48 AM
 */

/*
 * 
 */
#include<main.h>

enum{
    LED_OFF,
    LED_ON
};

enum{
    LED_1,
    LED_2,
    LED_3        
};

typedef struct{
    uint8_t u8_LED_NAME;
    uint8_t u8_LED_pin;  //BIT
    uint8_t c_LED_port;  //PORT 
    uint8_t u8_LED_status;  //LED on or off
}led_struct_t;

led_struct_t LED1_struct;
led_struct_t LED2_struct;
led_struct_t LED3_struct;

#define FOSC 96E6
#define CORE_TICK_PERIOD (FOSC/2)
   


int main(void) {
  LED1_struct.u8_LED_NAME=LED_1;
   LED1_struct.c_LED_port='E';
    LED1_struct.u8_LED_pin = BIT_4;
    LED1_struct.u8_LED_status = LED_OFF;
    
    LED2_struct.u8_LED_NAME=LED_2;
    LED2_struct.c_LED_port = 'E';
    LED2_struct.u8_LED_pin = BIT_6;
    LED2_struct.u8_LED_status = LED_OFF;

    LED3_struct.u8_LED_NAME=LED_3;
    LED3_struct.c_LED_port = 'E';
    LED3_struct.u8_LED_pin = BIT_7;
    LED3_struct.u8_LED_status = LED_OFF;

  
  
  
   UART1_init(115200);
        UART1_write_string("[PIC]: Initializing...\r\n");
  //leds green, red, blue
  mPORTBSetPinsDigitalOut(BIT_2);
    mPORTBSetPinsDigitalOut(BIT_3);
    mPORTBSetPinsDigitalOut(BIT_10);   
    //leds 1,2,3
    mPORTESetPinsDigitalOut(LED1_struct.u8_LED_pin);
        mPORTESetPinsDigitalOut(LED2_struct.u8_LED_pin);
        mPORTESetPinsDigitalOut(LED3_struct.u8_LED_pin);
        
    //Switch1
        mPORTDSetPinsDigitalIn(BIT_6);
        
    mPORTBClearBits(BIT_2);
    mPORTBClearBits(BIT_3);
    mPORTBClearBits(BIT_10);
    
    LED_on(LED1_struct.u8_LED_NAME);
    LED_off( LED2_struct.u8_LED_NAME);
    LED_on(LED3_struct.u8_LED_NAME);
    
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 // STEP 1. configure the core timer
 OpenCoreTimer(CORE_TICK_PERIOD);
 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 // STEP 2. set core timer interrupt level = 2
 mConfigIntCoreTimer(CT_INT_ON | CT_INT_PRIOR_2);
 //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 // STEP 3. enable multi-vector interrupts
 INTEnableSystemMultiVectoredInt();
 
 
 
 while(1){
 if(PORTDbits.RD6)
            LED_off(LED1_struct.u8_LED_NAME);
        else
            LED_on(LED1_struct.u8_LED_NAME);
 };
}



void __ISR(_CORE_TIMER_VECTOR, ipl2) _CoreTimerHandler(void)
{
 // clear the interrupt flag
 mCTClearIntFlag();

 // update the period
 UpdateCoreTimer(CORE_TICK_PERIOD);

 // toggle led 2
 
 LED_toggle(LED2_struct.u8_LED_NAME);
}
           


void LED_on(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTESetBits(LED1_struct.u8_LED_pin);
            break;
        case LED_2:
            mPORTESetBits(LED2_struct.u8_LED_pin);
            break;
        case LED_3:
            mPORTESetBits(LED3_struct.u8_LED_pin );
            break;
        default:
            break;
    }
}
void LED_off(uint8_t LED){
    switch (LED) {
        case LED_1:
            mPORTEClearBits(LED1_struct.u8_LED_pin);
            break;
        case LED_2:
            mPORTEClearBits(LED2_struct.u8_LED_pin);
            break;
        case LED_3:
            mPORTEClearBits(LED3_struct.u8_LED_pin );
            break;
        default:
            break;
    }
}
void LED_toggle(uint8_t LED){
    switch (LED){
        case LED_1:
            mPORTEToggleBits(LED1_struct.u8_LED_pin);
            break;
        case LED_2:
            mPORTEToggleBits(LED2_struct.u8_LED_pin);
            break;
     case LED_3:
            mPORTEToggleBits(LED3_struct.u8_LED_pin );
            break;
        default:
            break;
    }
}